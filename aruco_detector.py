import pickle

import cv2
import numpy as np
import cv2.aruco as aruco
import yaml


class ArucoDetector:
    def __init__(self, config):
        self._dict = config["aruco"]["dict"]
        self._aruco_markers_count = config["aruco"]["count"]

    def get_features_from_camera(self, image) -> dict:
        if image is None:
            return {"ok": False, "features": []}
        return {"ok": True, "features": self.__find_aruco_markers(image)}

    def __find_aruco_markers(self, img) -> list:
        imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        arucoDict = aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
        arucoParam = cv2.aruco.DetectorParameters()
        bboxs, ids, rej = aruco.detectMarkers(imgGray,
                                              arucoDict, parameters=arucoParam)
        feature_current_positions = []

        if (ids is not None and len(ids) > 0):
            sort_markers = {}
            for i in range(len(ids)):
                sort_markers[ids[i][0]] = bboxs[i]
            keys = list(sort_markers.keys())
            keys.sort()
            sorted_dict = {i: sort_markers[i] for i in keys}
            bboxs = list(sorted_dict.values())

            if len(bboxs) == self._aruco_markers_count:
                if self._aruco_markers_count == 1:
                    feature_current_positions = [bboxs[0][0][0], bboxs[0][0][1], bboxs[0][0][2], bboxs[0][0][3]]
                else:
                    for i in range(self._aruco_markers_count):
                        center = [bboxs[i][0][0], bboxs[i][0][1], bboxs[i][0][2], bboxs[i][0][3]]
                        feature_current_positions.append(center)
        return np.array(feature_current_positions)

    def show_features(self, image):
        data = self.get_features_from_camera(image)
        if data["ok"] and len(data["features"]) == 4:
            for i, pos in enumerate(data["features"].reshape((-1, 2))):
                pos = [int(x) for x in pos]
                cv2.putText(image, str(i+1), pos, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        return cv2.resize(image, (300, 300))

class CameraPositions:

    def __init__(self, config):
        self.k = np.array(config["camera"]["k"])
        self.dist = np.array(config["camera"]["dist"])
        self.aruco_pos = np.array(config["aruco"]["pos"], dtype="float32")
        self.Aruco = ArucoDetector(config)

    def camera_calibration(self, imgs):
        objpoints = []
        imgpoints = []

        for img in imgs:
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            data = self.Aruco.get_features_from_camera(img)
            if data["ok"]:
                objpoints.append(self.aruco_pos)
                print(data["features"])
                aruco = data["features"].reshape((config["aruco"]["count"] * 4, 1, 2))
                imgpoints.append(aruco)

        print(len(objpoints), len(imgpoints))
        ret, k, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

        self.k = k
        self.dist = dist

    def save_calibration(self, yaml_path):

        with open(yaml_path) as f:
            config = yaml.safe_load(f)

        config["camera"]["k"] = self.k.tolist()
        config["camera"]["dist"] = self.dist.tolist()

        with open(yaml_path, 'w') as f:
            yaml.dump(config, f)

    def get_transpose_matrix(self, img):

        arr = np.array([[0, 0, 0, 1]])
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        data = self.Aruco.get_features_from_camera(img)
        if data["ok"] and len(data["features"]) == config["aruco"]["count"]:
            aruco = data["features"][0].reshape((4, 1, 2))
            ret, rvecs, tvecs = cv2.solvePnP(self.aruco_pos, aruco, self.k, self.dist, 0)
            try:
                T = np.vstack([np.hstack([cv2.Rodrigues(rvecs)[0], tvecs]), arr])
            except IndexError:
                return -1
            return T

        return -1


if __name__ == "__main__":
    with open("config/config.yaml", "r") as c:
        config = yaml.safe_load(c)

    det = ArucoDetector(config)
    cal = CameraPositions(config)
    cap = cv2.VideoCapture("aruco.mp4")
    p = np.array([0, 0, 0]).reshape(3, 1)
    while True:
        ret, img = cap.read()
        if not ret:
            break
        pos = det.get_features_from_camera(img)
        T = cal.get_transpose_matrix(img)
        det.show_features(img)
        if type(T)==type(-1):
            continue
        p = np.hstack([p, T[:3, -1].reshape(3, 1)])
        print(pos)
        cv2.imshow("v", img)
        cv2.waitKey(1)
    """fig = go.Figure(data=[go.Scatter3d(x=p[0, :],
                                    y=p[1, :],
                                    z=p[2, :])])
    fig.show()"""
    print(p)

