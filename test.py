import cv2
import numpy as np
import cv2 as cv
import glob


# termination criteria
frames = 300
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((3*3,3), np.float32)
objp[:,:2] = np.mgrid[0:3,0:3].T.reshape(-1,2)*26.25
# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
cap = cv.VideoCapture("calib.mp4")
g = 0
print(objp)
arr = np.array([[0, 0, 0, 1]])
cm = np.array([[1171.44597579089, 0.0, 636.2251393996463],
               [0.0, 1181.0368341925882, 372.466015011798],
               [0.0, 0.0, 1.0]])
dist = np.array([[0.026302392084613692, 0.27361504573604073,
                  -6.19833039395515e-06, -0.001275044848848057, -3.861495450056694]])
zer = np.zeros((frames, 4, 4))
i=0
while True:
    res, img = cap.read()
    if g % 1 != 0:
        g += 1
        continue
    if not res or g > frames:
        break
    print(g)
    g += 1
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (3, 3), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        corners2 = cv.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
        ret, rvecs, tvecs = cv.solvePnP(objp, corners2, cm, dist, 0)
        mtx = np.array([np.vstack([np.hstack([cv2.Rodrigues(rvecs)[0], tvecs.reshape((3, 1))]), arr]) for i in
                        range(len(rvecs))])
        zer[i, :, :] = np.linalg.inv(mtx[0])
        # Draw and display the corners
        for i in range(corners2.shape[0]):
            cv2.putText(img, str(i+1), [int(x) for x in corners2[i, 0, :]], cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 2)
        cv.imshow('img', img)
        cv.waitKey(10)
        i += 1

    if i>=frames:
        break
print(zer)
cv.destroyAllWindows()
